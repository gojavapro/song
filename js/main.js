const CHORD_NOTATIONS = {
		NONE: "none", 
		LETTER: "Letter", 
		SOLFAGE: "Solfage"
}
const CHORDS = {
	C: {
		SOLFAGE: "Do", 
		LETTER: "C"
		},		
	D: {
		SOLFAGE: "Re",
		LETTER: "D"
		},
	E: {
		SOLFAGE: "Mi",
		LETTER: "E" 
		},
	F: {
		SOLFAGE: "Fa", 
		LETTER: "F" 
		},
	G: {
		SOLFAGE: "Sol", 
		LETTER: "G" 
		},
	A: {
		SOLFAGE: "La",
		LETTER: "A" 
		},
	B: {
		SOLFAGE: "Si",
		LETTER: "B" 
		}
}
var config={notation: CHORD_NOTATIONS.SOLFAGE}; 
$(window).ready(function() {
	s1=new Song( 
			"Renuevame", 
			 ["//Renuevame señor Jesús", "ya no quiero ser igual"], 
			 [[{pos:2, chord: new Chord (CHORDS.D, "", "", "")}, 
			   {pos:9, chord: new Chord (CHORDS.G, "", "", "")},
			   {pos:14,chord: new Chord (CHORDS.A, "", "", "")}, 
			   {pos:20,chord: new Chord (CHORDS.D, "", "", "")}],
			  [{pos:2, chord: new Chord (CHORDS.G, "", "", "")}, 
			   {pos:8, chord: new Chord (CHORDS.E, "m", "", "")},
			   {pos:14,chord: new Chord (CHORDS.A, "", "", "")},
			   {pos:16,chord: new Chord (CHORDS.A, "", "", "7")}]], 
		     100, 
		     "balada");
	s1.display();
});
let s1="";

function submitSong() {
	let song_text = $("#song_entry").val();
	song_text = song_text.replace("\t","    ");
	$("#song_entry").val(song_text);	
	let song_split = song_text.split("\n");
	let song_name = song_split[0];
	song_name = song_name.trim();
	song_name = song_name.replaceAll(" ","_");
	$("#song_name").val(song_name);
	//console.log(song_split);
	let data = $("#song_form").serialize();
	$.ajax({
		  type: "POST",
		  url: "readSong.php",
		  data: data,
		  success: function(respData){
			  alert("The Song " + respData.name + " was " + respData.message);
			  console.log(respData)},
		  dataType: "JSON",
		  error:function(){alert("an error has ocurred");}
		});
}


