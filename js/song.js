function Song (name, text, chords, tempo, rithm) {
	this.name = name;
	this.text = text;
	this.chords = chords; 
	this.tempo = tempo;
	this.rithm = rithm;
	this.display = function(){
		let c = [];
		$("#song_title").html(this.name); 
		for(let index in this.text) {
			if(config.notation != CHORD_NOTATIONS.NONE) {
				// cord line
				let lineChords = [];	
				lineChords.length = this.text[index].length; 
				lineChords.fill("&nbsp;");
				for (let chordsIndex in this.chords[index]){
					chordobj = this.chords[index][chordsIndex];
					if (chordobj.pos <= lineChords.length) {
						lineChords[chordobj.pos] = chordobj.chord;
					}
				}
				c.push("<p class='chord'>");
				c.push(lineChords.join(""));
				c.push("</p>");
			}
			c.push("<p class='song'>"); 
			c.push(this.text[index]);
			c.push("</p>");
			$("#song_content").html(c.join(""));
		}
	}
}	