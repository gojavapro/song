function Chord (key, minor, sharp, octave) {
	this.key=key;
	this.minor = minor;
	this.sharp= sharp;
	this.octave- octave;
	this.toString = function () {			
		if (config.notation == CHORD_NOTATIONS.NONE) {
			return;
		}
		let c = [];
		if (config == null && config.notation == CHORD_NOTATIONS.SOLFAGE ) {
			c.push(this.key.SOLFAGE);
		}else{
			c.push(this.key.LETTER);
		}
		c.push(sharp);
		c.push(minor);
		c.push(octave);
		return c.join("");
	}
}