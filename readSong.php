<?php 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $song_name = $_POST['song_name'];
    $song_text = $_POST['song_entry'];
    if (empty($song_text)) {
        echo "Song is empty";
    } else {
        header('Content-Type: application/json');
        echo "{\"name\":\"";
        echo $song_name;
        echo "\", \"message\": \"succesfully saved\"}";
        $myfile = fopen(dirname(__FILE__)."/songs/" . $song_name . ".txt", "w") or die("Unable to open file!");       
        fwrite($myfile, $song_text); 
        fclose($myfile);
    }
}
?>
 
